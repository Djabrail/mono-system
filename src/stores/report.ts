import { defineStore } from "pinia";

export const useReportStore = defineStore({
  id: "report",
  state: () => ({
    report: [],
    count: 0,
  }),
  getters: {},
  actions: {},
});
